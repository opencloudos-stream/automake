%global __requires_exclude ^perl\\(Automake::
%global __provides_exclude ^perl\\(Automake::
%global _configure_gnuconfig_hack 0
%global __brp_mangle_shebangs_exclude_from /usr/share/automake-1.16

Summary:    A tool for automatically generating 'Makefile.in' files
Name:       automake
Version:    1.16.5
Release:    7%{?dist}
License:    GPLv2+ and GFDL and Public Domain and MIT
URL:        https://www.gnu.org/software/%{name}/
Source0:    https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Source1:    http://git.savannah.gnu.org/cgit/config.git/plain/config.sub
Source2:    http://git.savannah.gnu.org/cgit/config.git/plain/config.guess

BuildRequires:  autoconf coreutils findutils make automake
BuildRequires:  perl-generators perl-interpreter perl(Thread::Queue) perl(threads)
# For test suite
BuildRequires: help2man bison cscope dejagnu emacs expect flex gcc-gfortran gcc-c++
BuildRequires: gettext-devel java-11-openjdk-devel libtool sharutils
Requires:   autoconf perl(Thread::Queue) perl(threads)
BuildArch:  noarch

%description
Automake is a tool for automatically generating 'Makefile.in' files
for use with Autoconf, compliant with the GNU Makefile
standards, and portable to various make implementations.

%prep
%autosetup
autoreconf -iv
cp m4/acdir/README README.aclocal
cp contrib/multilib/README README.multilib
cp %{SOURCE1} lib/config.sub
cp %{SOURCE2} lib/config.guess

%build
%configure
%make_build

%install
%make_install

%check
make check %{?_smp_mflags}

%files
%license COPYING*
%doc AUTHORS README THANKS NEWS README.aclocal README.multilib
%doc %{_docdir}/%{name}/amhello-1.0.tar.gz
%{_datadir}/automake-*
%{_datadir}/aclocal-*
%exclude %{_datadir}/aclocal
%{_bindir}/*
%{_infodir}/*.info*
%exclude %{_infodir}/dir
%{_mandir}/man1/*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.16.5-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Mon Aug 19 2024 rockerzhu <rockerzhu@tencent.com> - 1.16.5-6
- Fix constant version in %files to * for auot upgrade.

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.16.5-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.16.5-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.16.5-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.16.5-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Apr 21 2022 Xiaojie Chen <jackxjchen@tencent.com> - 1.16.5-1
- Initial build
